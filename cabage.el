;;; cabage -- a minimalist 'package' retriever - definitely not a package manager.
;;; Commentary:
;;;  1/ Installation:
;;;    + From source code:
;;;      Copy cabage.el in to your load path, and load it.
;;;    + Or in case you don't want to:
;;;      Just copy the code into your .emacs or init.el.
;;;  2/ Usage:
;;;    + Please make sure git is installed.
;;;    + Simply do this to get the package you want:
;;;      (get-cabage "dash" "https://github.com/magnars/dash.el.git")
;;;    + Or in case it is a theme:
;;;      (get-cabage "gruvbox-theme" "https://github.com/greduan/emacs-theme-gruvbox.git" t)
;;;  3/ Cautions:
;;;    + the package names is the file that will be loaded, in above case dash.el must exist in repository root.
;;;    + the dependency is not resolved, you will have to do it yourself.
;;; License:
;;;    This program is free software: you can redistribute it and/or modify
;;;    it under the terms of the GNU General Public License as published by
;;;    the Free Software Foundation, either version 3 of the License, or
;;;    (at your option) any later version.
;;;
;;;    This program is distributed in the hope that it will be useful,
;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;    GNU General Public License for more details.
;;;
;;;    You should have received a copy of the GNU General Public License
;;;    along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;; Code:

;; start rethink the whole mess they called package manager
;; FUCK it !!!
(unless (file-directory-p "~/.emac.d/cabages")
  (let ((default-directory "~/.emacs.d/"))
    (shell-command-to-string "mkdir cabages")))

(defun get-cabage (name git-url &optional is-theme)
  "A much more simple way to get the package.
NAME: the package name to load.
GIT-URL: the git URL of package.
IS-THEME: the package is a theme, and should be treated as so."
  (unless (file-directory-p (concat "~/.emacs.d/cabages/" name))
    (let ((default-directory "~/.emacs.d/cabages/"))
      (shell-command (concat "mkdir " name)))
    (let ((default-directory (concat "~/.emacs.d/cabages/" name "/")))
      (shell-command "git init")
      (shell-command (concat "git remote add origin " git-url))
      (shell-command "git fetch origin master")
      (shell-command "git merge origin/master")))
  (add-to-list 'load-path (concat "~/.emacs.d/cabages/" name "/") t)
  (if is-theme
      (add-to-list 'custom-theme-load-path (concat "~/.emacs.d/cabages/" name "/"))
    (require (intern name))))

(provide 'cabage) ; in case you decide to copy code, ignore this line
;;; cabage.el ends here
