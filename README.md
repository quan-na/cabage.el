# About Cabage
Cabage is a minimalist 'package' retriever - definitely not a package manager.

# Installation

## From source code
Copy cabage.el in to your load path, and load it.
> (require 'cabage)

## Or in case you don't want to
Just copy the code and paste into your .emacs or init.el.

# Usage
 * Please make sure git is installed.
 * Simply do this to get the package you want:

> (get-cabage "dash" "https://github.com/magnars/dash.el.git")

 * Or in case it is a theme:

> (get-cabage "gruvbox-theme" "https://github.com/greduan/emacs-theme-gruvbox.git" t)

# Cautions:
 * the package name is the file that will be loaded, in above case dash.el must exist in repository root.
 * the dependency is not resolved, you will have to do it yourself.

# License
>    This program is free software: you can redistribute it and/or modify
>    it under the terms of the GNU General Public License as published by
>    the Free Software Foundation, either version 3 of the License, or
>    (at your option) any later version.
>
>    This program is distributed in the hope that it will be useful,
>    but WITHOUT ANY WARRANTY; without even the implied warranty of
>    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
>    GNU General Public License for more details.
>
>    You should have received a copy of the GNU General Public License
>    along with this program.  If not, see <https://www.gnu.org/licenses/>.
